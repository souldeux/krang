import numpy, time, os
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K

class Brain:
    def __init__(self):
        #Fixed seed for numpy.random for reproducibility
        self.seed = 42
        numpy.random.seed(self.seed)

        self.X_train = None
        self.y_train = None
        self.X_test = None
        self.y_train = None

    def _load_data(self):
        #Load MNIST handwriting dataset
        #(self.X_train, self.y_train), (self.X_test, self.y_test) = mnist.load_data()
        brainPath = os.path.dirname(os.path.dirname(__file__))
        mnistPath = os.path.join(brainPath, 'datasets/mnist.npz')
        f = numpy.load(mnistPath)
        self.X_train, self.y_train = f['x_train'], f['y_train']
        self.X_test, self.y_test = f['x_test'], f['y_test']
        f.close()

    def _flatten(self):
        #Flatten 28*28 images into 784 vector
        num_pixels = self.X_train.shape[1] * self.X_train.shape[2]
        self.X_train = self.X_train.reshape(
            self.X_train.shape[0], num_pixels
            ).astype('float32')
        self.X_test = self.X_test.reshape(
            self.X_test.shape[0], num_pixels
            ).astype('float32')
        return num_pixels

    def _normalize(self):
        #Pixels are grayscale values from 0 to 255; normalize to 0-1
        self.X_train = self.X_train / 255
        self.X_test = self.X_test / 255

    def _one_hot_encode_outputs(self):
        self.y_train = np_utils.to_categorical(self.y_train)
        self.y_test = np_utils.to_categorical(self.y_test)
        return self.y_test.shape[1]

    def _reshape(self):
        self.X_train = self.X_train.reshape(
            self.X_train.shape[0], 1, 28, 28
            ).astype('float32')
        self.X_test = self.X_test.reshape(
            self.X_test.shape[0], 1, 28, 28
            ).astype('float32')

    def baseline_mlp(self):
        self._load_data()
        self.num_pixels = self._flatten()
        self.num_classes = self._one_hot_encode_outputs()
        self._normalize()

        model = Sequential()
        model.add(
            Dense(
                self.num_pixels,
                input_dim = self.num_pixels,
                kernel_initializer = 'normal',
                activation = 'relu'
                )
            )
        model.add(
            Dense(
                self.num_classes,
                kernel_initializer = 'normal',
                activation = 'softmax'
                )
            )
        model.compile(
            loss = 'categorical_crossentropy',
            optimizer = 'adam',
            metrics = ['accuracy']
            )
        return model

    def simple_cnn(self):
        K.set_image_dim_ordering('th')
        self._load_data()
        self._reshape()
        self._normalize()
        self.num_classes = self._one_hot_encode_outputs()

        model = Sequential()
        model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dense(self.num_classes, activation='softmax'))
        model.compile(
            loss = 'categorical_crossentropy',
            optimizer = 'adam',
            metrics = ['accuracy']
            )
        return model

    def _randomized_values(self):
        '''
        Variable values:
            - Feature maps in convolutional layer #1
            - Feature maps in convolutional layer #2
            - Dropout probability
            - Number of neurons in fully-connected layer #1
            - Number of neurons in fully-connected layer #2
            - Number of neurons in fully-connected layer #3
        '''
        #randomize seed
        numpy.random.seed(int(time.time()))
        _array = numpy.random.randint(1, 100, size=6)
        valMap = {
            'maps1': _array[0],
            'maps2': _array[1],
            'drop_prob': _array[2],
            'fc_neurons1': _array[3],
            'fc_neurons2': _array[4],
            'fc_neurons3': _array[5],
            }
        #reset seed
        numpy.random.seed(self.seed)
        return valMap

    def randomized_cnn(self):
        K.set_image_dim_ordering('th')
        valMap = self._randomized_values()
        self._load_data()
        self._reshape()
        self._normalize()
        self.num_classes = self._one_hot_encode_outputs()
        model = Sequential()
        model.add(
            Conv2D(
                valMap['maps1'],
                (5, 5),
                input_shape=(1, 28, 28),
                activation = 'relu'
                )
            )
        model.add(
            MaxPooling2D(pool_size=(2, 2))
            )
        model.add(
            Conv2D(
                valMap['maps2'],
                (3, 3),
                activation='relu'
                )
            )
        model.add(
            MaxPooling2D(pool_size=(2,2))
            )
        model.add(Dropout(valMap['drop_prob']/100))
        model.add(Flatten())
        model.add(Dense(valMap['fc_neurons1'], activation='relu'))
        model.add(Dense(valMap['fc_neurons2'], activation='relu'))
        model.add(Dense(valMap['fc_neurons3'], activation='relu'))
        model.add(Dense(self.num_classes, activation='softmax'))

        model.compile(
            loss = 'categorical_crossentropy',
            optimizer = 'adam',
            metrics = ['accuracy']
            )
        return model

    def run(self, model_name, num_epochs=10, batch_size=200):
        model = getattr(self, model_name)()

        if any(i is None for i in [
            self.X_train,
            self.y_train,
            self.X_test,
            self.y_test
                ]):
            raise RuntimeError(
                "All training & testing arrays must be populated before fitting."
                )

        history = model.fit(
            self.X_train,
            self.y_train,
            validation_data = (self.X_test, self.y_test),
            epochs = num_epochs,
            batch_size = batch_size,
            verbose = 2
            )
        scores = model.evaluate(self.X_test, self.y_test, verbose=0)
        print("Baseline Error: {0:.2f}".format(100-scores[1]*100))
        return scores, history
