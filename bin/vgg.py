#!/usr/bin/env python

'''
Forces the VGG19 data to download if not already present
'''
from keras.applications.vgg19 import VGG19
__ = VGG19()
