FROM ubuntu:xenial

ARG display

RUN mkdir /opt/krang
COPY . /opt/krang
WORKDIR /opt/krang

RUN apt-get update && \
	apt-get install -y gunicorn3 && \
	apt-get install -y curl && \
	apt-get install -y python3-tk && \
	apt-get install -y python3-pip

RUN pip3 install --upgrade pip && \
	hash -r pip && \
	pip3 install --upgrade pip setuptools && \
	pip3 install -r /opt/krang/requirements.txt && \
	rm -rf /root/.cache/pip

RUN python3 -m compileall -f -q .

#RUN python3 /opt/krang/bin/vgg.py

EXPOSE 5000
#CMD ["gunicorn3", "-b", "0.0.0.0:5000", "--reload", "--workers=2", "--log-level=debug", "app:app"]

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF_8
CMD ["flask", "run", "--host=0.0.0.0"]
