# Keras / Tensorflow

### Master

[![pipeline status](https://gitlab.com/souldeux/krang/badges/master/pipeline.svg)](https://gitlab.com/souldeux/krang/commits/master)

### Edge

[![pipeline status](https://gitlab.com/souldeux/krang/badges/edge/pipeline.svg)](https://gitlab.com/souldeux/krang/commits/edge)

---
# Local Usage Note

If necessary, set up `socat` listener with `socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"`
  - This will allow you to get display output from container (matplotlib GUI stuff).
  - Check `docker.env` for the display environment variable and adjust as necessary.
  - This, obviously, isn't terribly useful when using this as part of a web service.


# As a web/containerized service:

Meant to run with the `kerrigan` frontend.

`docker network create -d bridge overmind` to create the bridge network
