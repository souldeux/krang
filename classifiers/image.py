from keras.applications import vgg19
from keras.preprocessing.image import load_img, img_to_array
from keras import backend as K

class ImageClassifier:
    def __init__(self, img_path=None):
        self.set_img(img_path)
        self.imageArray = None

    def set_img(self, img_path):
        self.image = load_img(img_path, target_size=(224, 224)) if img_path is not None else None
        return True

    def preprocess_image(self):
        if self.image is None:
            raise Exception("self.image must be set!")

        #Convert the pixels to a NumPy array. Since we have only one image,
        #but the network expects a 4D array (samples, rows, columns, channels),
        #we will reshape to indicate we have a single sample
        self.imageArray = img_to_array(self.image)
        self.imageArray = self.imageArray.reshape((
            1,
            self.imageArray.shape[0],
            self.imageArray.shape[1],
            self.imageArray.shape[2]
        ))
        self.imageArray = vgg19.preprocess_input(self.imageArray)
        return True

    def predict(self):
        if self.imageArray is None:
            #raise Exception("self.imageArray must be set!")
            self.preprocess_image()

        model = vgg19.VGG19()
        predictions = model.predict(self.imageArray)
        labels = vgg19.decode_predictions(predictions)
        #Must clear model & session, or tensorflow will say it can't
        #interpret the stale feed_dict keys as Tensors
        del model
        K.clear_session()
        return labels

    def test(self):
        self.set_img('/opt/krang/classifiers/assets/mug.jpg')
        return self.predict()
