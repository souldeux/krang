from flask import Flask, Response, render_template, request
import brains, classifiers, json, tempfile
from PIL import Image
from decimal import Decimal

app = Flask(__name__, template_folder='templates')

@app.route('/', methods=['GET'], strict_slashes=False)
def index():
	return render_template('index.html')

@app.route('/health', methods=['GET'], strict_slashes=False)
def health():
	return 'ok', 200

@app.route('/architecture/<string:brainType>/', methods=['GET'], strict_slashes=False)
def architecture(brainType):
	b = brains.Brain()
	try:
		model = getattr(b, brainType)()
	except:
		return 'invalid brainType \
			(valid: simple_cnn, randomized_cnn, baseline_mlp)', 400
	path = "architecture.html"
	return render_template(path, repr=model.to_yaml())

def _save_to_tempfile(request):
	tf = tempfile.NamedTemporaryFile(delete=False, suffix='.jpg')
	tf.write(request.files['file'].read())
	tf.close()
	filename = tf.name
	tf = None
	return filename

@app.route('/classify', methods=['POST'], strict_slashes=False)
def classify():
	fn = _save_to_tempfile(request)
	ic = classifiers.ImageClassifier(fn)
	predictions = ic.predict()[0]
	return json.dumps(dict([(str(p[1]), '{:,.4f}'.format(Decimal(str(p[2]))) ) for p in predictions])), 200

@app.after_request
def add_cors_headers(response):
	#If the request is from Eyeball, inject headers to allow access
	r = request.environ.get('HTTP_ORIGIN')
	if r in ['http://localhost:80', 'http://0.0.0.0:8080']:
		injectables = [
			('Access-Control-Allow-Origin', r),
			('Access-Control-Allow-Credentials', 'true'),
			('Access-Control-Allow-Headers', 'Content-Type'),
			('Access-Control-Allow-Headers', 'Cache-Control'),
			('Access-Control-Allow-Headers', 'X-Requested-With'),
			('Access-Control-Allow-Headers', 'Authorization'),
			('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE'),
		]
		for i in injectables:
			response.headers.add(i[0], i[1])
	return response


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)
